module Main where

import Control.Monad
import Control.Monad.Reader
import Data.HSet
import Data.Proxy
import Test.HUnit

hset :: HSet '[Int, Double, Integer, Float]
hset = HSCons 1
      $ HSCons 2
      $ HSCons 3
      $ HSCons 4
      $ HSNil

hsetEq :: Bool
hsetEq = hset == hset

hsetCompare :: Ordering
hsetCompare = compare hset hset

hsetInt :: Int
hsetInt = hget hset

hsetDoube :: Double
hsetDoube = hget hset

hsetInteger :: Integer
hsetInteger = hget hset

hsetFloat :: Float
hsetFloat = hget hset

this :: (Double, Float)
this =
  let h = (,) <$> hask <*> hask
  in runReader h hset

data Label = This | That

labeledHSet :: HSet '[Labeled 'This Int, Labeled 'That Int, Labeled "hello" String]
labeledHSet = HSCons (Labeled 1)
              $ HSCons (Labeled 2)
              $ HSCons (Labeled "olleh")
              HSNil

main :: IO ()
main = void $ runTestTT $ TestList
    [ TestList
      [ TestCase $ hsetEq @?= True
      , TestCase $ hsetCompare @?= EQ
      , TestCase $ hsetInt @?= 1
      , TestCase $ hsetDoube @?= 2
      , TestCase $ hsetInteger @?= 3
      , TestCase $ hsetFloat @?= 4
      ]
    , TestList
      [ TestCase $ (hgetLabeled (Proxy :: Proxy 'This) labeledHSet) @?= (1 :: Int)
      , TestCase $ (hgetLabeled (Proxy :: Proxy 'That) labeledHSet) @?= (2 :: Int)
      , TestCase $ (hgetLabeled (Proxy :: Proxy "hello") labeledHSet) @?= "olleh"
      ]
    ]
