module Data.HSet.Labeled where

import Data.Typeable

newtype Labeled (label :: k) (typ :: *) = Labeled { unLabeled :: typ }
    deriving ( Typeable, Show, Eq, Ord )

mkLabel :: proxy label -> e -> Labeled label e
mkLabel _ = Labeled
