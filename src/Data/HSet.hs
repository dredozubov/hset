module Data.HSet
       ( HSet(..)
       , HGet(..)
       , HGettable
       , hask
       , HModify(..)
       , HModifiable
       , HMonoModifiable
       , hMonoModify
       , hGetState
       , hPutState
       , hModifyState
       , SubHSet(..)
       , SubHSetable
       , hnarrow
       , HRemove(..)
       , hdelete
       , HUnion(..)
       , HUnionable
         -- * Work with 'Labeled' elements
       , hgetLabeled
       , haskLabeled
         -- * Reexports
       , module Data.HSet.Labeled
       ) where

import Control.Monad.Reader
import Control.Monad.State
import Data.HSet.Labeled
import Data.HSet.TypeLevel
import Data.Typeable

#if !(MIN_VERSION_base(4, 8, 0))
import Control.Applicative
#endif

{- | Heterogeneous set (list) of elements with unique types. Useful
with MonadReader.

>>> let x = HSCons (10 :: Int) $ HSCons (20 :: Double) HSNil
>>> x
HSCons (10) (HSCons (20.0) (HSNil))

>>> hget x :: Int
10

>>> hget x :: Double
20.0

Note that 'hget' takes specific element from list of uniquely typed
elements depending on what type is required to be returned.

-}

data HSet (elems :: [*]) where
  HSNil  :: HSet '[]
  HSCons :: ('False ~ (Elem elem elems)) => !elem -> HSet elems -> HSet (elem ': elems)
  deriving ( Typeable )

instance Show (HSet '[]) where
  show HSNil = "HSNil"

instance (Show e, Show (HSet els)) => Show (HSet (e ': els)) where
  show (HSCons e els) = "HSCons (" ++ show e ++ ") (" ++ show els ++ ")"

instance Eq (HSet '[]) where
  HSNil == HSNil = True

instance (Eq e, Eq (HSet els)) => Eq (HSet (e ': els)) where
  (HSCons e els) == (HSCons e' els') = (e == e') && (els == els')

instance Ord (HSet '[]) where
  HSNil `compare` HSNil = EQ

instance (Ord e, Ord (HSet els)) => Ord (HSet (e ': els)) where
  (HSCons e els) `compare` (HSCons e' els') = case e `compare` e' of
    EQ -> els `compare` els'
    x  -> x

-- | Typeclass for sets and elements.
class (i ~ (Index e els)) => HGet els e i | els i -> e where
  -- | Gets any data from HSet for you
  hget :: HSet els -> e

instance HGet (e ': els) e 'Z where
  hget (HSCons e _) = e

instance (('S i) ~ (Index e (e1 ': els)), HGet els e i)
         => HGet (e1 ': els) e ('S i) where
  hget (HSCons _ els) = hget els

-- | Enables deriving of the fact that 'e' is contained within 'els' and it's
-- safe to say that 'hget' can be performed on this particular pair.
type HGettable els e = HGet els e (Index e els)

hask :: (MonadReader (HSet els) m, HGettable els e) => m e
hask = do
  h <- ask
  return $ hget h

class (i ~ (Index e1 els1), i ~ (Index e2 els2))
      => HModify els1 els2 e1 e2 i
       | els1 i     -> e1  , els2 i     -> e2
       , els1 e1 e2 -> els2, els2 e1 e2 -> els1 where
  hmodify :: (e1 -> e2) -> HSet els1 -> HSet els2

instance ('False ~ Elem e2 els)
         => HModify (e1 ': els) (e2 ': els) e1 e2 'Z where
  hmodify f (HSCons e els) = HSCons (f e) els

instance ( ('S i) ~ (Index e1 (ex ': els1))
         , ('S i) ~ (Index e2 (ex ': els2))
         , HModify els1 els2 e1 e2 i
         , 'False ~ (Elem ex els2) )
         => HModify (ex ': els1) (ex ': els2) e1 e2 ('S i) where
  hmodify f (HSCons e els) = HSCons e $ hmodify f els


-- | Check that we can turn one hset to another
type HModifiable els1 els2 e1 e2 = HModify els1 els2 e1 e2 (Index e1 els1)


-- | Helper type infering that hset 'els' contains element of type 'e'
-- and can be modified
type HMonoModifiable els e = HModify els els e e (Index e els)


-- | Like 'hmodify' but do not change the hset's type
hMonoModify :: HMonoModifiable els e
            => (e -> e)
            -> HSet els
            -> HSet els
hMonoModify f h = hmodify f h

hGetState :: ( MonadState (HSet els) m
             , HGettable els e )
          => m e
hGetState = do
  h <- get
  return $ hget h

hPutState :: ( MonadState (HSet els) m
             , HMonoModifiable els e )
           => e -> m ()
hPutState e = do
  h <- get
  put $ hMonoModify (const e) h


hModifyState :: ( MonadState (HSet els) m
                , HMonoModifiable els e )
             => (e -> e) -> m ()
hModifyState f = modify (hMonoModify f)

{- | Takes subset of some hset, including subset of same elements in
different order

>>> let x = (HSCons "hello" $ HSCons 1234 $ HSCons 12.123 HSNil) :: HSet '[String, Int, Double]

>>> subHSet x :: HSet '[Double, Int]
HSCons (12.123) (HSCons (1234) (HSNil))

>>> subHSet x :: HSet '[String, Double]
HSCons ("hello") (HSCons (12.123) (HSNil))

>>> subHSet x :: HSet '[Int, String]
HSCons (1234) (HSCons ("hello") (HSNil))

-}

class (eq ~ TEq els els2)
      => SubHSet els els2 eq where
  subHSet :: HSet els -> HSet els2

instance (eq ~ TEq els '[]) => SubHSet els '[] eq where
  subHSet _ = HSNil

instance ( HGettable els el, 'False ~ Elem el els2
         , SubHSet els els2 subeq
         , 'False ~ TEq els (el ': els2) )
         => SubHSet els (el ': els2) 'False where
  subHSet h = HSCons (hget h :: el) (subHSet h :: HSet els2)

instance ( HGettable els el, 'False ~ Elem el els2
         , SubHSet els els2 subeq
         , els ~ (el ': els2)
         , 'True ~ TEq els (el ': els2) )
         => SubHSet els (el ': els2) 'True where
  subHSet h = h


type SubHSetable els1 els2 = (SubHSet els1 els2 (TEq els1 els2))


-- | Remove i's element from hset. Second argument is a resulting hset
-- type
class HRemove els1 els2 i | els1 i -> els2 where
  hremove :: Proxy i -> HSet els1 -> HSet els2

instance HRemove (e ': els) els 'Z where
  hremove _ (HSCons _ els) = els

instance ( 'False ~ (Elem e els2)
         , HRemove els1 els2 i )
         => HRemove (e ': els1) (e ': els2) ('S i) where
  hremove _ (HSCons e els) = HSCons e $ hremove (Proxy :: Proxy i) els

{- | Delete element from HSet of specified type

>>> let x = (HSCons "sdf" $ HSCons 123 HSNil) :: HSet '[String, Int]

>>> hdelete (Proxy :: Proxy Int) x
HSCons ("sdf") (HSNil)

>>> hdelete (Proxy :: Proxy String) x
HSCons (123) (HSNil)

-}

hdelete :: forall proxy els1 els2 e.
        (HRemove els1 els2 (Index e els1))
        => proxy e -> HSet els1 -> HSet els2
hdelete _ h = hremove (Proxy :: Proxy (Index e els1)) h

class ( fidx ~ (MayFstIndexSnd els1 els2)
      , sidx ~ (MayFstIndexSnd els2 els1) )
      => HUnion els1 els2 elsr fidx sidx
       | els1 els2 fidx sidx -> elsr where
  hunion :: HSet els1 -> HSet els2 -> HSet elsr

instance HUnion '[] '[] '[] 'Nothing 'Nothing where
  hunion _ _ = HSNil

instance HUnion '[] (e ': els) (e ': els) 'Nothing 'Nothing where
  hunion _ a = a

instance HUnion (e ': els) '[] (e ': els) 'Nothing 'Nothing where
  hunion a _ = a

instance ( HUnionable els1 els2 elsr
         , 'False ~ (Elem e1 (e2 ': elsr))
         , 'False ~ (Elem e2 elsr)
         , 'Nothing ~ (MayFstIndexSnd (e1 ': els1) (e2 ': els2))
         , 'Nothing ~ (MayFstIndexSnd (e2 ': els2) (e1 ': els1)) )
         => HUnion (e1 ': els1) (e2 ': els2) (e1 ': e2 ': elsr) 'Nothing 'Nothing where
  hunion (HSCons e1 els1) (HSCons e2 els2) = HSCons e1 $ HSCons e2 $ hunion els1 els2

instance ( HUnionable els1 els2 elsr
         , 'False ~ (Elem e elsr) )
         => HUnion (e ': els1) (e ': els2) (e ': elsr) ('Just 'Z) ('Just 'Z) where
  hunion (HSCons e els1) (HSCons _ els2) = HSCons e $ hunion els1 els2

instance ( HRemove els2 elsx fi
         , HUnionable els1 elsx elsr
         , 'False ~ (Elem e1 elsr)
         , ('Just ('S fi)) ~ (MayFstIndexSnd (e1 ': els1) (e2 ': els2))
         , ('Just si) ~ (MayFstIndexSnd (e2 ': els2) (e1 ': els1)) )
         => HUnion (e1 ': els1) (e2 ': els2) (e1 ': elsr) ('Just ('S fi)) ('Just si) where
  hunion (HSCons e1 els1) (HSCons _ els2) = HSCons e1 $ hunion els1 $ hremove (Proxy :: Proxy fi) els2

instance ( HRemove els2 elsx fi
         , HUnionable els1 elsx elsr
         , 'False ~ (Elem e1 elsr)
         , ('Just fi) ~ (MayFstIndexSnd (e1 ': els1) els2)
         , 'Nothing ~ (MayFstIndexSnd els2 (e1 ': els1)) )
         => HUnion (e1 ': els1) els2 (e1 ': elsr) ('Just fi) 'Nothing where
  hunion (HSCons e1 els1) els2 = HSCons e1 $ hunion els1 $ hremove (Proxy :: Proxy fi) els2

instance ( HUnionable els1 els2 elsr
         , 'False ~ (Elem e1 elsr)
         , 'Nothing ~ (MayFstIndexSnd (e1 ': els1) (e2 ': els2))
         , ('Just si) ~ (MayFstIndexSnd (e2 ': els2) (e1 ': els1)) )
         => HUnion (e1 ': els1) (e2 ': els2) (e1 ': elsr) 'Nothing ('Just si) where
  hunion (HSCons e1 els1) (HSCons _ els2) = HSCons e1 $ hunion els1 els2

type HUnionable els1 els2 elsr =
  HUnion els1 els2 elsr (MayFstIndexSnd els1 els2) (MayFstIndexSnd els2 els1)

{- | Like 'subHSet' but with proxy for convenience

>>> let x = (HSCons "hello" $ HSCons 123 $ HSCons 345 HSNil) :: HSet '[String, Int, Integer]

>>> hnarrow (Proxy :: Proxy '[]) x
HSNil

>>> hnarrow (Proxy :: Proxy '[String]) x
HSCons ("hello") (HSNil)

>>> hnarrow (Proxy :: Proxy '[Int, Integer]) x
HSCons (123) (HSCons (345) (HSNil))

>>> hnarrow (Proxy :: Proxy '[Integer, Int]) x
HSCons (345) (HSCons (123) (HSNil))

-}

hnarrow :: (SubHSetable els subels)
        => proxy subels -> HSet els -> HSet subels
hnarrow _ = subHSet



{- |

>>> let y = HSCons (Labeled 10 :: Labeled "x" Int) $ HSCons (Labeled 20 :: Labeled "y" Int) HSNil
>>> y
HSCons (Labeled {unLabeled = 10}) (HSCons (Labeled {unLabeled = 20}) (HSNil))

>>> hgetLabeled (Proxy :: Proxy "x") y :: Int
10

>>> hgetLabeled (Proxy :: Proxy "y") y :: Int
20

-}

hgetLabeled :: forall proxy label e els.
            (HGettable els (Labeled label e))
            => proxy label -> HSet els -> e
hgetLabeled _ hset =
  let x = hget hset
  in unLabeled (x :: Labeled label e)

haskLabeled :: forall proxy label e els m.
#if MIN_VERSION_base(4, 8, 0)
            (HGettable els (Labeled label e), MonadReader (HSet els) m)
#else
            (HGettable els (Labeled label e), MonadReader (HSet els) m, Applicative m)
#endif
            => proxy label -> m e
haskLabeled p = hgetLabeled p <$> ask
